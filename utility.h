#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

/** Struct usata per passare informazioni al thread segnalatore */
typedef struct
{
	std::string * serverAddress;	//server:port delle web api
	std::string * raspberryUUID;	//uuid del raspberry
	std::string * dateTime;			//timestamp della richiesta
	std::vector<uchar> *imgData;	//byte del frame
} SEG_PARAMS;

const std::string serverEndPoint = "/api/PhotoFiles";

/* Lettura della libreria dei volti */
void read_csv(const std::string& filename, std::vector<cv::Mat>& images, std::vector<int>& labels, std::map<int, std::string>& labelsInfo, char separator);

/* Invio della segnalazione al web service */
void *send_segnalazione(void *threadarg);

/* Get RaspberryPi Unique Serial Number from /proc/cpuinfo */
std::string getSerialNumber();

/* Get current date/time, format is YYYY-MM-DD HH:mm:ss */
std::string currentDateTime();